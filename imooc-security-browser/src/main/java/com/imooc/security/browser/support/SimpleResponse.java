package com.imooc.security.browser.support;

/**
 * @author yangdc
 * @date 2018/9/25 上午1:40
 */
public class SimpleResponse {
    private Object content;

    public SimpleResponse(Object content) {
        this.content = content;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
