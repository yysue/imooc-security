package com.imooc.security.browser.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @author yangdc
 * @date 2018/9/5 19:50
 */
@Service
public class MyUserDetailsService implements UserDetailsService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("根据用户名查找用户信息");
        logger.info("用户名：" + username);
        // 假设从数据库查询相关的密码和权限
        String password = passwordEncoder.encode("123456");
        // 假设只要用户名为admin2018就能正常登录
        boolean enabled = "admin2018".equals(username);
        logger.info("密码: " + password);
        return new User(username, // 用户名
                password, // 这个是从数据库中读取的已加密的密码
                enabled, // 是否可用
                true, // 账号是否过期
                true, // 密码是否过期
                true, // 账号没有被锁定标志
                AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
    }
}
