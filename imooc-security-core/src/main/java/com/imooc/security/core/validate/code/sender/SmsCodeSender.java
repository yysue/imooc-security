package com.imooc.security.core.validate.code.sender;

/**
 * 短信验证码发送器接口
 *
 * @author yangdc
 * @date 2018/9/26 上午12:47
 */
public interface SmsCodeSender {
    void send(String mobile, String code);
}
