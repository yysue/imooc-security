package com.imooc.security.core.properties;

/**
 * 浏览器属性
 *
 * @author yangdc
 * @date 2018/9/25 上午1:48
 */
public class BrowserProperties {
    // imooc.security.browser.loginPage
    private String loginPage = "/imooc-signIn.html";

    // imooc.security.browser.loginType
    private LoginResponseType loginType = LoginResponseType.JSON;

    // imooc.security.browser.rememberMeSeconds
    private int rememberMeSeconds = 3600;

    public int getRememberMeSeconds() {
        return rememberMeSeconds;
    }

    public void setRememberMeSeconds(int rememberMeSeconds) {
        this.rememberMeSeconds = rememberMeSeconds;
    }

    public String getLoginPage() {
        return loginPage;
    }

    public void setLoginPage(String loginPage) {
        this.loginPage = loginPage;
    }

    public LoginResponseType getLoginType() {
        return loginType;
    }

    public void setLoginType(LoginResponseType loginType) {
        this.loginType = loginType;
    }
}
