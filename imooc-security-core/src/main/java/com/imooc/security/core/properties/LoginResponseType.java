package com.imooc.security.core.properties;

/**
 * 登录后的响应方式
 *
 * @author yangdc
 * @date 2018/9/25 上午2:38
 */
public enum LoginResponseType {
    REDIRECT,
    JSON
}
