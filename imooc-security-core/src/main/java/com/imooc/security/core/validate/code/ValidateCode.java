package com.imooc.security.core.validate.code;

import java.time.LocalDateTime;

/**
 * 短信验证码
 *
 * @author yangdc
 * @date 2018/9/26 上午12:36
 */
public class ValidateCode {
    // 验证码
    private String code;
    // 过期时间
    private LocalDateTime expireTime;

    /**
     * @param code 验证码
     * @param expireIn 多长时间过期,单位秒
     */
    public ValidateCode(String code, int expireIn) {
        this(code, LocalDateTime.now().plusSeconds(expireIn));
    }

    /**
     * @param code 验证码
     * @param expireTime 过期时间
     */
    public ValidateCode(String code, LocalDateTime expireTime) {
        this.code = code;
        this.expireTime = expireTime;
    }

    /**
     * 验证码是否过期
     *
     * @return
     */
    public boolean isExpried() {
        return LocalDateTime.now().isAfter(expireTime);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }
}
