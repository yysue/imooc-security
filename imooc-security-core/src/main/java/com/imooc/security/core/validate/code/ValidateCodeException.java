package com.imooc.security.core.validate.code;


import org.springframework.security.core.AuthenticationException;

/**
 * @author yangdc
 * @date 2018/9/25 上午4:02
 */
public class ValidateCodeException extends AuthenticationException {
    public ValidateCodeException(String msg) {
        super(msg);
    }
}
