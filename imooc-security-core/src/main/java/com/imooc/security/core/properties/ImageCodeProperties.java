package com.imooc.security.core.properties;

/**
 * 图形验证码属性
 *
 * @author yangdc
 * @date 2018/9/25 上午4:34
 */
public class ImageCodeProperties extends SmsCodeProperties {
    private int width = 67;
    private int height = 23;

    public ImageCodeProperties() {
        setLength(4); // 图形验证码长度默认4
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
