package com.imooc.security.core.validate.code.sender;

/**
 * 默认的短信验证码发送器实现
 *
 * @author yangdc
 * @date 2018/9/26 上午12:50
 */
public class DefaultSmsCodeSender implements SmsCodeSender {
    @Override
    public void send(String mobile, String code) {
        System.out.println("向手机" + mobile + "发送短信验证码" + code);
    }
}
