package com.imooc.security.core.validate.code;

import com.imooc.security.core.properties.ImoocSecurityConstants;
import com.imooc.security.core.validate.code.generator.ValidateCodeGenerator;
import com.imooc.security.core.validate.code.processor.ValidateCodeProcessor;
import com.imooc.security.core.validate.code.processor.ValidateCodeProcessorHolder;
import com.imooc.security.core.validate.code.sender.SmsCodeSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author yangdc
 * @date 2018/9/25 上午3:20
 */
@RestController
public class ValidateCodeController {
    private Logger logger = LoggerFactory.getLogger(ValidateCodeController.class);

    @Autowired
    private ValidateCodeProcessorHolder validateCodeProcessorHolder;

    /**
     * 创建验证码,根据验证码类型不同,调用不同的{@link ValidateCodeProcessor}接口实现
     *
     * @param request
     * @param response
     * @param type image表示返回图片/sms表示发送短信
     * @throws Exception
     */
    @GetMapping("/code/{type}")
    public void createCode(HttpServletRequest request,
                           HttpServletResponse response,
                           @PathVariable String type) throws Exception {
        validateCodeProcessorHolder.findValidateCodeProcessor(type)
                .create(new ServletWebRequest(request, response));
    }
}
