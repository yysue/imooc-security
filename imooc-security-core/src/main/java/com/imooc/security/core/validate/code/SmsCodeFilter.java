//package com.imooc.security.core.validate.code;
//
//import com.imooc.security.core.properties.SecurityProperties;
//import com.imooc.security.core.validate.code.processor.ValidateCodeProcessorHolder;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.web.authentication.AuthenticationFailureHandler;
//import org.springframework.util.AntPathMatcher;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.HashSet;
//import java.util.Set;
//
///**
// * @author yangdc
// * @date 2018/9/25 上午3:58
// */
//public class SmsCodeFilter extends OncePerRequestFilter implements InitializingBean {
//    private Logger logger = LoggerFactory.getLogger(SmsCodeFilter.class);
//
//    private AuthenticationFailureHandler authenticationFailureHandler;
//
//    private Set<String> urls = new HashSet<>();
//
//    private SecurityProperties securityProperties;
//
//    private AntPathMatcher antPathMatcher = new AntPathMatcher();
//
//    @Autowired
//    private ValidateCodeProcessorHolder validateCodeProcessorHolder;
//
//    public void setSecurityProperties(SecurityProperties securityProperties) {
//        this.securityProperties = securityProperties;
//    }
//
//    @Override
//    public void afterPropertiesSet() throws ServletException {
//        super.afterPropertiesSet();
//        urls.add("/authentication/mobile"); // 处理登录的请求一定要验证
//    }
//
//    @Override
//    protected void doFilterInternal(HttpServletRequest request,
//                                    HttpServletResponse response,
//                                    FilterChain filterChain)
//            throws ServletException, IOException {
//        System.out.println(request.getMethod() + "\t" + request.getRequestURI());
//
//        boolean action = false;
//        for (String url : urls) {
//            if (antPathMatcher.match(url, request.getRequestURI())) {
//                action = true;
//            }
//        }
//
//        if (action) {
//            // && StringUtils.endsWithIgnoreCase("post", request.getMethod())
//            try {
////                validate(new ServletWebRequest(request));
//                logger.info("对验证码进行验证");
//            } catch (ValidateCodeException e) {
//                authenticationFailureHandler.onAuthenticationFailure(request, response, e);
//                return;
//            }
//        }
//
//        filterChain.doFilter(request, response);
//    }
//
//    public void setAuthenticationFailureHandler(AuthenticationFailureHandler authenticationFailureHandler) {
//        this.authenticationFailureHandler = authenticationFailureHandler;
//    }
//}
