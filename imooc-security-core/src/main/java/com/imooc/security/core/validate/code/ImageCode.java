package com.imooc.security.core.validate.code;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * 图片验证码
 *
 * @author yangdc
 * @date 2018/9/25 上午3:16
 */
public class ImageCode extends ValidateCode {
    // 验证码图片
    private BufferedImage image;

    /**
     * @param image    图片
     * @param code     验证码
     * @param expireIn 多长时间过期,单位秒
     */
    public ImageCode(BufferedImage image, String code, int expireIn) {
        this(image, code, LocalDateTime.now().plusSeconds(expireIn));
    }

    /**
     * @param image      图片
     * @param code       验证码
     * @param expireTime 过期时间
     */
    public ImageCode(BufferedImage image, String code, LocalDateTime expireTime) {
        super(code, expireTime);
        this.image = image;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }
}
