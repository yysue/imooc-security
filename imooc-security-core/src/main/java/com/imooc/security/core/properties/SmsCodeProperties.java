package com.imooc.security.core.properties;

/**
 * 短信验证码属性
 *
 * @author yangdc
 * @date 2018/9/26 上午1:03
 */
public class SmsCodeProperties {
    private int length = 6; // 短信验证码长度默认6
    private int expireIn = 60; // 默认60秒
    private String url; // 多个url时用逗号隔开

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(int expireIn) {
        this.expireIn = expireIn;
    }
}
