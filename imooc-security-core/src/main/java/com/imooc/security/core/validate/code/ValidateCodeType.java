package com.imooc.security.core.validate.code;

import com.imooc.security.core.properties.ImoocSecurityConstants;

/**
 * 验证码类型
 *
 * @author yangdc
 * @date 2018/9/26 上午1:54
 */
public enum ValidateCodeType {
    SMS {
        @Override
        public String getParamNameOnValidate() {
            return ImoocSecurityConstants.DEFAULT_PARAMETER_NAME_CODE_SMS;
        }
    },
    IMAGE {
        @Override
        public String getParamNameOnValidate() {
            return ImoocSecurityConstants.DEFAULT_PARAMETER_NAME_CODE_IMAGE;
        }
    };

    public abstract String getParamNameOnValidate();
}
