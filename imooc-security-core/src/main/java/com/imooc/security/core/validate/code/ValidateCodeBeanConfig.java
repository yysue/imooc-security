package com.imooc.security.core.validate.code;

import com.imooc.security.core.properties.SecurityProperties;
import com.imooc.security.core.validate.code.generator.ImageCodeGenerator;
import com.imooc.security.core.validate.code.generator.ValidateCodeGenerator;
import com.imooc.security.core.validate.code.sender.DefaultSmsCodeSender;
import com.imooc.security.core.validate.code.sender.SmsCodeSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 用来初始化可配置的一些属性
 *
 * @author yangdc
 * @date 2018/9/25 上午5:10
 */
@Configuration
public class ValidateCodeBeanConfig {

    @Autowired
    private SecurityProperties securityProperties;

    // 定义图片验证码生成器
    @Bean
    @ConditionalOnMissingBean(name = "imageValidateCodeGenerator")
    public ValidateCodeGenerator imageValidateCodeGenerator() {
        System.out.println("init default imageValidateCodeGenerator");
        ImageCodeGenerator codeGenerator = new ImageCodeGenerator();
        codeGenerator.setSecurityProperties(securityProperties);
        return codeGenerator;
    }

    // 定义短信验证码发送器
    @Bean
    @ConditionalOnMissingBean(SmsCodeSender.class)
    public SmsCodeSender smsCodeSender() {
        System.out.println("init default smsCodeSender");
        return new DefaultSmsCodeSender();
    }
}
