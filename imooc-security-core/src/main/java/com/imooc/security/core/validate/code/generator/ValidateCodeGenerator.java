package com.imooc.security.core.validate.code.generator;

import com.imooc.security.core.validate.code.ValidateCode;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 验证码生成器接口
 *
 * @author yangdc
 * @date 2018/9/25 上午5:08
 */
public interface ValidateCodeGenerator {
    ValidateCode generate(ServletWebRequest request);
}
