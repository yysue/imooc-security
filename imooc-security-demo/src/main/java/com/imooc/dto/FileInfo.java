package com.imooc.dto;

/**
 * @author yangdc
 * @date 2018/9/3 3:15
 */
public class FileInfo {
    private String filePath;

    public FileInfo(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
