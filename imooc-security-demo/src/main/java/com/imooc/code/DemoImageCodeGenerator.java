//package com.imooc.code;
//
//import com.imooc.security.core.properties.SecurityProperties;
//import com.imooc.security.core.validate.code.ImageCode;
//import com.imooc.security.core.validate.code.generator.ValidateCodeGenerator;
//import org.apache.commons.lang3.RandomStringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//import org.springframework.web.bind.ServletRequestUtils;
//import org.springframework.web.context.request.ServletWebRequest;
//
//import java.awt.*;
//import java.awt.image.BufferedImage;
//import java.util.Random;
//
///**
// * 自定义 验证码图片生成器
// *
// * @author yangdc
// * @date 2018/9/25 上午5:15
// */
//@Component("imageCodeGenerator")
//public class DemoImageCodeGenerator implements ValidateCodeGenerator {
//    @Autowired
//    private SecurityProperties securityProperties;
//
//    private static Random random = new Random();
//
//    @Override
//    public ImageCode generate(ServletWebRequest request) {
//        String sRand = RandomStringUtils.randomAlphanumeric(
//                securityProperties.getCode().getImage().getLength());
//
//        int width = ServletRequestUtils.getIntParameter(request.getRequest(), "width",
//                securityProperties.getCode().getImage().getWidth());
//        int height = ServletRequestUtils.getIntParameter(request.getRequest(), "height",
//                securityProperties.getCode().getImage().getHeight());
//
//        // 在内存中创建图象
//        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//        Graphics2D g = bi.createGraphics();
//        for(int i=0; i<sRand.length(); i++){
//            Color color = getRandomColor();
//            Color reverse = getReverseColor(color);
//            g.setColor(color);    //设置字体颜色
//            g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 25));    //设置字体样式
//            g.fillRect(0, 0, width, height);
//            g.setColor(reverse);
//            g.drawString(sRand, 18, 25);
//        }
//        //随机生成一些点
//        for (int i = 0, n = random.nextInt(100); i < n; i++) {
//            g.drawRect(random.nextInt(width), random.nextInt(height), 1, 1);
//        }
//        // 随机产生干扰线，使图象中的认证码不易被其它程序探测到
//        for (int i = 0; i < 10; i++) {
//            g.setColor(getRandomColor());
//            final int x = random.nextInt(width-1); // 保证画在边框之内
//            final int y = random.nextInt(height-1);
//            final int xl = random.nextInt(width);
//            final int yl = random.nextInt(height);
//            g.drawLine(x, y, x + xl, y + yl);
//        }
//        g.dispose();    //图像生效
//        System.out.println("生成自定义的图形验证码:" + sRand);
//        return new ImageCode(bi, sRand, securityProperties.getCode().getImage().getExpireIn());
//    }
//
//    /**
//     * 获取随机数颜色
//     *
//     * @return
//     */
//    private static Color getRandomColor() {
//        return new Color(random.nextInt(255),random.nextInt(255), random.nextInt(255));
//    }
//
//    /**
//     * 返回某颜色的反色
//     *
//     * @param c
//     * @return
//     */
//    private static Color getReverseColor(Color c) {
//        return new Color(255 - c.getRed(), 255 - c.getGreen(), 255 - c.getBlue());
//    }
//}
