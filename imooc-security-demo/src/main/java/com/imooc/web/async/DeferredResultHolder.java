package com.imooc.web.async;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.HashMap;
import java.util.Map;

/**
 * 用于在应用1/线程1与应用1/线程2之间传递传递DeferredResult对象
 * @author yangdc
 * @date 2018/9/4 0:03
 */
@Component
public class DeferredResultHolder {
    // 订单号，订单处理结果
    private Map<String, DeferredResult<String>> map = new HashMap<>();

    public Map<String, DeferredResult<String>> getMap() {
        return map;
    }

    public void setMap(Map<String, DeferredResult<String>> map) {
        this.map = map;
    }
}
