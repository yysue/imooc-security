package com.imooc.web.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 模拟队列
 * @author yangdc
 * @date 2018/9/4 0:00
 */
@Component
public class MockQueue {
    private String placeOrder; // 下单消息
    private String completeOrder; // 订单完成订单完成

    private Logger logger = LoggerFactory.getLogger(getClass());

    public String getPlaceOrder() {
        return placeOrder;
    }

    public void setPlaceOrder(String placeOrder) {
        // 此线程是模拟应用2，处理下单逻辑
        new Thread(() -> {
            logger.info("接到下单请求：" + placeOrder);
            try {
                Thread.sleep(1000); // 模拟处理下单过程
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.completeOrder = placeOrder;
            logger.info("下单请求处理完毕：" + placeOrder);
        }).start();
    }

    public String getCompleteOrder() {
        return completeOrder;
    }

    public void setCompleteOrder(String completeOrder) {
        this.completeOrder = completeOrder;
    }
}
