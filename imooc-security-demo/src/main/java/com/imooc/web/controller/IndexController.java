package com.imooc.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author yangdc
 * @date 2018/10/11 上午3:08
 */
@Controller
public class IndexController {

    // 设置网站默认首页
    @GetMapping("/")
    public String defaultIndex() {
        return "forward:/index.html";
    }
}
