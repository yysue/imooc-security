package com.imooc.web.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author yangdc
 * @date 2018/9/3 2:45
 */
@Aspect
@Component
public class TimeAspect {
    // @Before
    // @After
    // @AfterThrowing
    // @Around

    // https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#aop-pointcuts-combining
    @Around("execution(* com.imooc.web.controller.UserController.*(..))")
    public Object handlerControllerMethod(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("===|===|===time aspect start");
        Object[] args = pjp.getArgs();
        for (Object arg : args) {
            System.out.println("===|===|===arg is " + arg);
        }
        long start = new Date().getTime();
        Object object = pjp.proceed();
        System.out.println("===|===|===time aspect 耗时:" + (new Date().getTime() - start));
        System.out.println("===|===|===time aspect end");
        return object;
    }
}
