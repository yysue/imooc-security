package com.imooc.exception;

/**
 * @author yangdc
 * @date 2018/9/3 1:50
 */
public class UserNotExistException extends  RuntimeException {
    private String id;
    public UserNotExistException(String id) {
        super("user not exist");
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
