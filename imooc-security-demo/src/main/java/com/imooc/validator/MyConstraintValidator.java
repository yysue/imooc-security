package com.imooc.validator;

import com.imooc.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * A: 对Annotation的一个验证的实现
 * T : 这个Annotation声明的字段的类型
 * @author yangdc
 * @date 2018/9/3 0:26
 */
public class MyConstraintValidator implements ConstraintValidator<MyConstraint, Object> {

    @Autowired
    HelloService helloService;

    @Override
    public void initialize(MyConstraint constraintAnnotation) {
        System.out.println("myConstraint validator init");
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        System.out.println("valid " + value + " invoke helloService.greeting "
                + helloService.greeting("leo"));
        // return true;
        return false;
    }
}
