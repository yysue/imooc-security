package com.imooc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author yangdc
 * @date 2018/8/26 3:19
 */
@SpringBootApplication
@ServletComponentScan // 用于扫描@WebFilter
@EnableSwagger2 // 启用Swagger2
public class DemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
