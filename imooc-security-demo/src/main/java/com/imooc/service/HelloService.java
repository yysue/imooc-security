package com.imooc.service;

/**
 * @author yangdc
 * @date 2018/9/3 0:29
 */
public interface HelloService {
    String greeting(String name);
}
