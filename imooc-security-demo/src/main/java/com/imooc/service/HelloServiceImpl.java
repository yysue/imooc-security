package com.imooc.service;

import org.springframework.stereotype.Service;

/**
 * @author yangdc
 * @date 2018/9/3 0:29
 */
@Service
public class HelloServiceImpl implements HelloService {
    @Override
    public String greeting(String name) {
        return "hello " + name;
    }
}
