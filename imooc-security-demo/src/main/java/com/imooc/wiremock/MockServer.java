package com.imooc.wiremock;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

/**
 * @author yangdc
 * @date 2018/9/4 20:07
 */
public class MockServer {
    public static void main(String[] args) throws IOException {
        configureFor("192.168.5.210", 9999);
        // configureFor(9999);
        removeAllMappings();

        mock("/order/1", "01.txt");
        mock("/order/2", "02.txt");
    }

    private static void mock(String url, String fileName) throws IOException {
        ClassPathResource resource = new ClassPathResource("mock/response/" + fileName);
        String content = StringUtils.join(FileUtils.readLines(resource.getFile(), "UTF-8"), "\n");
        stubFor(get(urlPathEqualTo(url))
                .willReturn(aResponse().withBody(content).withStatus(200)));
    }
}
